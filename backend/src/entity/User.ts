import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

export enum UserRole {
    DEFAULT = "default",
    DEVELOPER = "developer"
}

@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id!: number;

    @Column()
    firstName!: string;

    @Column()
    lastName!: string;

    @Column()
    email!: string;

    @Column({
        type: "enum",
        enum: UserRole,
        default: UserRole.DEFAULT
    })
    role!: UserRole
}
