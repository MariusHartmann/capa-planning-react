import { Request, Response } from "express";
import {Connection} from "typeorm";
import Db from "../utils/dbManager";
import {User} from "../entity/User";
import { ControllerBase, GET } from '@egodigital/express-controllers';

/**
 * /controllers/index.ts
 *
 * Base path: '/'
 */
export class Controller extends ControllerBase {
    @GET()
    public async index(req: Request, res: Response) {
        Db.exec((connection: Connection) => {
            connection.getRepository(User).find()
            .then(users => {
                res.send(users);
            })
            .catch(err => {
                res.send("error")
            })
        })
    }
}