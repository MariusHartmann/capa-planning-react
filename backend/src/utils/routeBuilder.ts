import fs from "fs";
import path from "path";
import { Express } from "express";

const controllerDir = __dirname + "/../controllers";
const routePrefix = "/api/";

export default function(app: Express, callback?: Function) {
    const basePath = path.resolve(controllerDir);

    fs.readdir(controllerDir, (err, files) => {
        if(err) return callback(err);

        files.forEach(async file => {
            file = path.resolve(controllerDir, file);

            let controller = await import(file)
            let route = file.replace(basePath, "").replace(".ts", "");
            route = route.slice(1, route.length);

            app.get(routePrefix + route, (req, res, next) => controller.default(req, res, next))
        })
    })
}