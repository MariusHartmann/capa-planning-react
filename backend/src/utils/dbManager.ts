import "reflect-metadata";
import {createConnection, Connection} from "typeorm";

export default class Database {
    private static connection: Connection;

    private static connect() {
        return new Promise((resolve, reject) => {
            if(Database.connection && Database.connection.isConnected) {
                resolve();
            }
            else {
                createConnection()
                    .then(con => {
                        Database.connection = con;
                        resolve();
                    })
                    .catch(err => {
                        console.error("### CONNECTION ERROR ###");
                        console.log(err)
                        reject();
                    })
            }
        })
    }

    public static disconnect() {
        Database.connection.close();
    }

    public static async exec(innerCode: Function) {
        Database.connect()
            .then(() => {
                innerCode(Database.connection);
            })
            .catch(err => {
                console.error("### CONNECTION ERROR ###");
                console.log(err)
            })
    }
}