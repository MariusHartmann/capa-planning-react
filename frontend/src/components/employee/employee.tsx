import React from "react";
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

import "./employee.scss";
import { IEmployee } from "../../views/employees/employees";

export interface IEmployeeProps {
    children?: any,
    employee: IEmployee
}

const Employee = (props: IEmployeeProps) => {
    const calcEffiency = () => {
        let effi = 100;

        if(props.employee.projects && props.employee.projects.length) {
            props.employee.projects.forEach((project: any, i: number) => {
                if(i > 0) {
                    effi = effi -= i * 5
                }
            });
        }

        return effi >= 0 ? effi : 0;
    }

    return (
        <Card raised={true} className="employee" variant="outlined">
            <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                    {props.employee.name} - <small>{calcEffiency()}% <small>Efficiency</small></small>
                </Typography>

                <ul>
                    {props.employee.projects.map(project => 
                        <li key={project.id}>{project.name}: ({project.capa} (-{project.capa - (project.capa * calcEffiency() / 100)} {'days lost'}))</li>
                    )}
                </ul>
            </CardContent>
        </Card>
    )
}

export default Employee;