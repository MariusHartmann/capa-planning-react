import React from "react";
import { useDispatch, useSelector } from 'react-redux';
import {
  Link
} from "react-router-dom";

import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { makeStyles, useTheme, Theme, createStyles } from '@material-ui/core/styles';

import "./navigation.scss";
import {show as showNav} from "../../store/actions/nav";

export interface INavigationProps {
  drawerWidth: number
}

const Navigation = (props: INavigationProps) => {
    const dispatch = useDispatch();

    const nav = useSelector((state: any) => state.nav);

    const classes = makeStyles((theme: Theme) =>
      createStyles({
        drawer: {
          [theme.breakpoints.up('sm')]: {
            width: props.drawerWidth,
            flexShrink: 0,
          }
        },
        drawerPaper: {
          width: props.drawerWidth,
        }
      })
    )();

    const onClose = () => {
      dispatch(showNav(false));
    };

    const drawer = (
      <List>
        <Link to="/" onClick={onClose}>
          <ListItem button key="home">
            <ListItemText primary="Home" />
          </ListItem>
        </Link>
        <Link to="/projects" onClick={onClose}>
          <ListItem button key="projects">
            <ListItemText primary="Projects" />
          </ListItem>
        </Link>
        <Link to="/employees" onClick={onClose}>
          <ListItem button key="employees">
            <ListItemText primary="Employees" />
          </ListItem>
        </Link>
      </List>
    );

    return (
      <nav className={classes.drawer}>
        <Hidden smUp implementation="css">
          <Drawer
            variant="temporary"
            open={nav}
            onClose={onClose}
            ModalProps={{
              keepMounted: true
            }}
            classes={{
              paper: classes.drawerPaper,
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer
            variant="permanent"
            open
            classes={{
              paper: classes.drawerPaper,
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
      </nav>
    )
}

export default Navigation;