import React from "react";
import { useDispatch, useSelector } from 'react-redux';

import MaterialAppBar from '@material-ui/core/AppBar';
import IconButton from '@material-ui/core/IconButton';
import Toolbar from '@material-ui/core/Toolbar';
import MenuIcon from '@material-ui/icons/Menu';

import "./appBar.scss";
import {show as showNav} from "../../store/actions/nav";
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

export interface IAppBarProps {
  drawerWidth: number
}

const AppBar = (props: IAppBarProps) => {
    const dispatch = useDispatch();

    const nav = useSelector((state: any) => state.nav);
    const classes = makeStyles((theme: Theme) =>
      createStyles({
        appBar: {
          [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${props.drawerWidth}px)`,
            marginLeft: props.drawerWidth,
          },
        },
        menuButton: {
          marginRight: theme.spacing(2),
          [theme.breakpoints.up('sm')]: {
            display: 'none',
          },
        }
      })
    )();

    const handleDrawerToggle = () => {
      dispatch(showNav(!nav));
    };

    return (
        <MaterialAppBar position="fixed" className={classes.appBar}>
          <Toolbar>
            <IconButton
              color="inherit"
              aria-label="open drawer"
              edge="start"
              onClick={handleDrawerToggle}
              className={classes.menuButton}
            >
              <MenuIcon />
            </IconButton>
          </Toolbar>
        </MaterialAppBar>
    )
}

export default AppBar;