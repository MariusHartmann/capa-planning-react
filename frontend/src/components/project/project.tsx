import React from "react";
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

import "./project.scss";
import { IProject } from "../../views/projects/projects";

export interface IProjectProps {
    children?: any,
    project: IProject
}

const Project = (props: IProjectProps) => {
    return (
        <Card className="project" variant="outlined">
            <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                    {props.project.name}
                </Typography>
                <Typography gutterBottom variant="subtitle1" component="h2">
                    <strong>Capa:</strong> {props.project.capa}
                </Typography>
            </CardContent>
        </Card>
    )
}

export default Project;