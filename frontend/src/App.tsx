import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import CssBaseline from '@material-ui/core/CssBaseline';
import { makeStyles, useTheme, Theme, createStyles } from '@material-ui/core/styles';

import "./App.scss"
import AppBar from "./components/appBar";
import Navigation from "./components/navigation";
import Home from './views/home';
import Projects from './views/projects';
import Employees from './views/employees';

import {set as setEmployees} from "./store/actions/employee";
import {set as setProjects} from "./store/actions/project";

const drawerWidth = 240;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
    },
    // necessary for content to be below app bar
    toolbar: theme.mixins.toolbar,
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
  }),
);

function App() {
  const dispatch = useDispatch();

  const classes = useStyles();
  
  useEffect(() => {
    loadEmployees();
    loadProjects();
  }, [])

  const loadEmployees = () => {
    fetch('/employees.json').then(response => {
      console.log(response);
      return response.json();
    }).then((data: any) => {
        dispatch(setEmployees(data))
    }).catch(err => {
      console.log("Error Reading data " + err);
    });
  }

  const loadProjects = () => {
    fetch('/projects.json').then(response => {
      console.log(response);
      return response.json();
    }).then((data: any) => {
        dispatch(setProjects(data))
    }).catch(err => {
      console.log("Error Reading data " + err);
    });
  }

  return (
    <div className={classes.root}>
      <CssBaseline />
      <Router>
        <AppBar drawerWidth={drawerWidth} />

        <Navigation drawerWidth={drawerWidth} />

        <main className={classes.content}>
          <div className={classes.toolbar} />
          <Switch>
            <Route path="/projects">
              <Projects />
            </Route>
            <Route path="/employees">
              <Employees />
            </Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
        </main>
      </Router>
    </div>
  );
}

export default App;