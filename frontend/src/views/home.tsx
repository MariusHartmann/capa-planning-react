import React, { useEffect } from 'react';
import axios from 'axios';

const Home = () => {
    useEffect(() => {
        axios.get("/api")
        .then(response => {
            console.log(response);
        })
    }, [])

    return (
        <div className="home">HOME</div>
    )
}

export default Home;