import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';


import "./projects.scss";
import Project from "../../components/project";

import { 
    add as addProject, 
    remove as removeProject 
} from "../../store/actions/project";

export interface IProject {
    id: string;
    name: string;
    capa: number;
}

const Projects = () => {
    const dispatch = useDispatch();

    const [ newProject, setNewProject ] = useState("");
    const projects = useSelector((state: any) => state.projects);

    const add = () => {
        dispatch(addProject({
            id: String(Math.random()),
            name: newProject,
            capa: 40
        }));

        // Input clear
        setNewProject("");
    }

    const remove = (index: number) => {
        dispatch(removeProject(index));
    }

    return (
        <Container className="projects">
            <Grid container spacing={3}>
                {projects.map((project: any, index: number) =>
                    <Grid item xs={12} sm={4} key={project.id}>
                        <Project project={project} ></Project>
                        <Button variant="contained" onClick={() => remove(index)}>Remove</Button>
                    </Grid >
                )}
                <input value={newProject} onChange={(e) => setNewProject(e.target.value)} />
                <Button variant="contained" onClick={() => add()}>Add</Button>
            </Grid>
        </Container>
    )
}

export default Projects;