import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

import "./employees.scss";
import Employee from "../../components/employee";

import { 
    add as addEmployee, 
    remove as removeEmployee 
} from "../../store/actions/employee";

export interface IEmployee {
    id: string;
    name: string;
    projects: any[];
}

const Employees = () => {
    const dispatch = useDispatch();
    
    const [ newEmployee, setNewEmployee ] = useState("");
    const employees = useSelector((state: any) => state.employees);

    const add = () => {
        dispatch(addEmployee({
            id: String(Math.random()),
            name: newEmployee,
            projects: []
        }));

        // Input clear
        setNewEmployee("");
    }

    const remove = (index: number) => {
        dispatch(removeEmployee(index));
    }

    return (
        <Container className="employees">
            <Grid container spacing={3}>
                {employees.map((employee: any, index: number) =>
                    <Grid item xs={12} sm={4} key={employee.id}>
                        <Employee employee={employee}></Employee>
                        <Button variant="contained" onClick={() => remove(index)}>Remove</Button>
                    </Grid>
                )}
                <input value={newEmployee} onChange={(e) => setNewEmployee(e.target.value)} />
                <Button variant="contained" onClick={() => add()}>Add</Button>
            </Grid>
        </Container>
    )
}

export default Employees;