const projectReducer = (state = [], action) => {
    switch(action.type) {
        case 'PROJECTS_SET':
            return action.meta.projects;
        case 'PROJECTS_ADD':
            return [
                ...state,
                action.meta.project
            ];
        case 'PROJECTS_REMOVE':
            return state.filter((_, i) => i !== action.meta.index);
        default:
            return state;
    }
}

export default projectReducer;