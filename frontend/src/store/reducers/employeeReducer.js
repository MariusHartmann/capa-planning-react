const employeeReducer = (state = [], action) => {
    switch(action.type) {
        case 'EMPLOYEE_SET':
            return action.meta.employees;
        case 'EMPLOYEE_ADD':
            return [
                ...state,
                action.meta.employee
            ];
        case 'EMPLOYEE_REMOVE':
            return state.filter((_, i) => i !== action.meta.index);
        default:
            return state;
    }
}

export default employeeReducer;