const navReducer = (state = false, action) => {
    switch(action.type) {
        case 'NAV_SHOW':
            return action.meta.show;
        default:
            return state;
    }
}

export default navReducer;