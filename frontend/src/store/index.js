import { createStore, combineReducers } from 'redux';
import employeeReducer from './reducers/employeeReducer';
import projectReducer from './reducers/projectReducer';
import navReducer from './reducers/navReducer';

const allReducers = combineReducers({
    employees: employeeReducer,
    projects: projectReducer,
    nav: navReducer
})

export default createStore(
    allReducers,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);