export const show = (show) => {
    return {
        type: 'NAV_SHOW',
        meta: {
            show
        }
    }
}

