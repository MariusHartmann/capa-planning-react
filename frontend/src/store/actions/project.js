export const set = (projects) => {
    return {
        type: 'PROJECTS_SET',
        meta: {
            projects
        }
    }
}


export const add = (project) => {
    return {
        type: 'PROJECTS_ADD',
        meta: {
            project
        }
    }
}
export const remove = (index) => {
    return {
        type: 'PROJECTS_REMOVE',
        meta: {
            index
        }
    }
}