export const set = (employees) => {
    return {
        type: 'EMPLOYEE_SET',
        meta: {
            employees
        }
    }
}

export const add = (employee) => {
    return {
        type: 'EMPLOYEE_ADD',
        meta: {
            employee
        }
    }
}
export const remove = (index) => {
    return {
        type: 'EMPLOYEE_REMOVE',
        meta: {
            index
        }
    }
}

